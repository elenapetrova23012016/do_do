let body = document.querySelector('.body-cont')
let createCard = function (card) {

    let newCard = document.createElement('div')
    newCard.classList.add('card')

    let title = document.createElement('h1')
    title.innerText = card.title;
    title.style.textAlign = "center"
    newCard.append(title)

    let img = document.createElement('img')
    img.src = card.url;
    newCard.append(img)

    let par = document.createElement('p')
    par.innerText = card.id;
    par.style.color = "orange"
    par.style.textAlign = "center"
    newCard.append(par)
    body.append(newCard)

    const changeColor = (event) => {
        const title = newCard.querySelector('h1')
        if (title.style.color == "black"){
            title.style.color = "red"
        }
        else{
            title.style.color = "black"
        }
    }
    newCard.addEventListener('click', changeColor)
}

fetch('https://jsonplaceholder.typicode.com/albums/1/photos')
    .then(response => response.json())
    .then(json => json.map(elem => createCard(elem)))
