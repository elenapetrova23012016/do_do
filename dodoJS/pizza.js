import { products } from "./productsArray.js";
console.log(products);

let sum = 0;
let shoppingCart = [];
let toppingArr = [];

let createPopUp = function (product) {
  let pizzaSize = "medium";
  let pizzaSizes = document.querySelectorAll(".dodo-popUp-input");
  function showPizzaSize(event) {
    pizzaSize = event.target.id;
    addedToppingsPrice = 0;
    makeToppings();
    calcPriceFromSize();
    setUpWeightSizeSpan();
    console.log(pizzaSize);
  }
  Array.from(pizzaSizes).map((item) =>
    item.addEventListener("click", showPizzaSize)
  );

  let pizzaDough = "traditional";
  let pizzaDoughes = document.querySelectorAll(".dodo-popUp-input-dough");
  function showPizzaDough(event) {
    pizzaDough = event.target.id;
    setUpDoughSpan();
    console.log(pizzaDough);
  }
  Array.from(pizzaDoughes).map((item) => {
    item.addEventListener("click", showPizzaDough);
  });

  //IIFE (function(){})()
  let productPopUp = document.querySelector(".dodo-product-popUp");
  productPopUp.style.display = "block";

  if (pizzaSize === "small") {
    document.getElementsByClassName("dodo-topping-input")[0].disabled = true;
  }

  if (pizzaDough === "thin") {
    document.getElementsByClassName("dodo-topping-input")[0].disabled = true;
  } else {
    document.getElementsByClassName("dodo-topping-input")[0].disabled = false;
  }

  let popupArrow = document.querySelector(".popup-arrow");
  popupArrow.addEventListener("click", function () {
    Array.from(pizzaSizes).map((item) => {
      item.removeEventListener("click", showPizzaSize);
    });
    Array.from(pizzaDoughes).map((item) => {
      item.removeEventListener("click", showPizzaDough);
    });
    productPopUp.style.display = "none";
  });

  // изменение изображения
  let popupImg = document.querySelector(".popupImg");
  popupImg.src = product.pizzaSizes[1].img.traditional;

  // id
  let productId = product.id;
  // изменение заголовка
  let popupTitle = document.querySelector(".dodo-product-popUp-header");
  popupTitle.innerText = product.title;

  let popupShortInfo = document.querySelector(".popup-shortInfo");
  popupShortInfo.innerHTML = "";
  let infoSpan1 = document.createElement("span");
  let infoSpan2 = document.createElement("span");
  let infoSpan3 = document.createElement("span");

  function setUpWeightSizeSpan() {
    if (pizzaSize === "small") {
      infoSpan1.innerText = `${product.pizzaSizes[0].size} см, `;
      infoSpan3.innerText = ` ${product.pizzaSizes[0].weight} гр.`;
    } else if (pizzaSize === "medium") {
      infoSpan1.innerText = `${product.pizzaSizes[1].size} см, `;
      infoSpan3.innerText = ` ${product.pizzaSizes[1].weight} гр.`;
    } else if (pizzaSize === "big") {
      infoSpan1.innerText = `${product.pizzaSizes[2].size} см, `;
      infoSpan3.innerText = ` ${product.pizzaSizes[2].weight} гр.`;
    }
  }
  setUpWeightSizeSpan();

  function setUpDoughSpan() {
    if (pizzaDough === "thin") {
      infoSpan2.innerText = ` ${product.dough[0]} , `;
    } else if (pizzaDough === "traditional") {
      infoSpan2.innerText = ` ${product.dough[1]} ,`;
    }
  }
  setUpDoughSpan();
  popupShortInfo.append(infoSpan1);
  popupShortInfo.append(infoSpan2);
  popupShortInfo.append(infoSpan3);

  let infoButton = document.querySelector(".dodo-info-button");

  let popupCalories = document.querySelector(".dodo-popup-calories");

  let popupCaloriesHeader = document.querySelector(
    ".dodo-popup-calories-header"
  );
  popupCaloriesHeader.innerText = "Пищевая ценность на 100 г";
  popupCalories.append(popupCaloriesHeader);

  // -----------------------Работает только 1 кнопка----------
  infoButton.addEventListener("click", function () {
    if (popupCalories.style.display == "none") {
      popupCalories.style.display = "block";
    } else {
      popupCalories.style.display = "none";
    }
  });

  let popupIngredients = document.querySelector(".pizza-ingredients");
  popupIngredients.innerHTML = "";
  product.ingredients.map((elem) => {
    let ingredientsButton = document.createElement("button");
    ingredientsButton.classList.add("popup-ingredientsButton");
    ingredientsButton.innerText = `${elem.name} `;

    if (elem.changeable === true) {
      ingredientsButton.classList.add("popup-ingredientsButton-dashed");
      ingredientsButton.innerText = `${elem.name} `;
      let ingredientsButtonImg = document.createElement("span");
      ingredientsButtonImg.classList.add("ingredientsButtonImg");
      ingredientsButton.append(ingredientsButtonImg);
      ingredientsButton.addEventListener("click", function () {
        if (
          ingredientsButton.classList.contains("ingredientsButton-excluded") ===
          true
        ) {
          ingredientsButton.classList.remove("ingredientsButton-excluded");
          ingredientsButton.classList.add("popup-ingredientsButton-dashed");
          ingredientsButtonImg.classList.remove("ingredientsButtonImg--focus");
          ingredientsButtonImg.classList.add("ingredientsButtonImg");
        } else {
          ingredientsButton.classList.add("ingredientsButton-excluded");
          ingredientsButtonImg.classList.add("ingredientsButtonImg--focus");
        }
        // let ingredientsButtonImg = document.querySelectorAll(
        //   ".ingredientsButtonImg"
        // );
      });
    }
    popupIngredients.append(ingredientsButton);
  });

  // отрисовка дополнительных ингредиентов
  let counter1 = 0;
  let toppingsCont = document.querySelector(".toppings--container");
  toppingArr = [];
  function makeToppings() {
    toppingsCont.innerHTML = "";
    product.additionalIngredients.map((ingredient) => {
      let sweetWrapper = document.createElement("button");
      sweetWrapper.classList.add("dodo-topping-sweetWrapper");
      toppingsCont.append(sweetWrapper);

      let toppingToggle = document.createElement("input");
      toppingToggle.classList.add("dodo-topping-input");
      // if (pizzaSize === "small") {
      //   document.getElementsByClassName(
      //     "dodo-topping-input"
      //   )[0].disabled = true;
      // }
      //
      // if (pizzaDough === "thin") {
      //   document.getElementsByClassName(
      //     "dodo-topping-input"
      //   )[0].disabled = true;
      // } else {
      //   document.getElementsByClassName(
      //     "dodo-topping-input"
      //   )[0].disabled = false;
      // }
      // if (toppingArr.includes(ingredient)) {
      //   toppingToggle.checked = true;
      // }

      toppingToggle.addEventListener("click", function () {
        console.log(ingredient);
        console.log(toppingToggle.id);
        if (toppingToggle.checked === true) {
          toppingArr.push(ingredient);
          calcPriceWithToppings();
          calcPriceFromSize();
        } else {
          toppingArr = toppingArr.filter(
            (item) => item.title !== ingredient.title
          );
          calcPriceWithToppings();
          calcPriceFromSize();
        }
        console.log(toppingArr);
      });

      toppingToggle.id = "topping__toggle" + counter1;
      toppingToggle.setAttribute("type", "checkbox");

      sweetWrapper.append(toppingToggle);

      let toppingLabel = document.createElement("label");
      toppingLabel.classList.add("dodo-topping-label");
      toppingLabel.setAttribute("for", "topping__toggle" + counter1);

      sweetWrapper.append(toppingLabel);
      counter1++;
      let toppingImg = document.createElement("img");
      toppingImg.classList.add("dodo-topping-img");
      toppingImg.src = ingredient.img;

      toppingLabel.append(toppingImg);

      let toppingHeader = document.createElement("h2");
      toppingHeader.classList.add("topping-header");
      toppingHeader.innerText = ingredient.title;

      toppingLabel.append(toppingHeader);

      let toppingPrice = document.createElement("p");
      toppingPrice.classList.add("topping-price");
      let priceSetUp = function () {
        if (pizzaSize === "small") {
          toppingPrice.innerText = `${ingredient.price.small}₽`;
        } else if (pizzaSize === "medium") {
          toppingPrice.innerText = `${ingredient.price.medium}₽`;
        } else {
          toppingPrice.innerText = `${ingredient.price.big}₽`;
        }
      };
      priceSetUp();
      toppingLabel.append(toppingPrice);
    });
  }
  makeToppings();

  //подсчет цены
  let addedToppingsPrice = 0;
  function calcPriceWithToppings() {
    addedToppingsPrice = 0;

    // if (pizzaSize === "small") {
    //   price = product.pizzaSizes[0].price;
    // } else if (pizzaSize === "medium") {
    //   price = product.pizzaSizes[1].price;
    // } else if (pizzaSize === "big") {
    //   price = product.pizzaSizes[2].price;
    // }
    toppingArr.forEach((topping) => {
      addedToppingsPrice = addedToppingsPrice + topping.price[pizzaSize];
    });
    // console.log(addedToppingsPrice);
  }

  let toBasketButton = document.querySelector(
    ".dodo-product-popUp-toBasket-button"
  );

  function calcPriceFromSize() {
    if (pizzaSize === "small") {
      toBasketButton.innerText = ` Добавить в корзину за ${
        product.pizzaSizes[0].price + addedToppingsPrice
      }  ₽`;
    } else if (pizzaSize === "medium") {
      toBasketButton.innerText = ` Добавить в корзину за ${
        product.pizzaSizes[1].price + addedToppingsPrice
      } ₽`;
    } else {
      toBasketButton.innerText = ` Добавить в корзину за ${
        product.pizzaSizes[2].price + addedToppingsPrice
      } ₽`;
    }
  }

  //создание объекта
  function CurrentProd(
    productId,
    title,
    img,
    dough,
    size,
    excludedIngredients,
    toppings,
    price,
    quantity
  ) {
    this.id = productId;
    this.title = title;
    this.img = img;
    this.dough = dough;
    this.size = size;
    this.excludedIngredients = Array.from(
      document.querySelectorAll(".ingredientsButton-excluded")
    ).map((item) => item.textContent);
    this.toppings = toppingArr;
    this.vendorCode =
      this.id +
      this.dough +
      this.size +
      this.excludedIngredients +
      this.toppings;
    this.price = Array.from(
      document.querySelectorAll(".dodo-product-popUp-toBasket-button")
    ).map((item) => item.innerText);
    this.quantity = 1;
    console.log(this.quantity);
  }

  calcPriceFromSize();

  //посылаем в корзину
  let stickyBasket = document.querySelector(".sticky-basket");
  let basketMeter = document.querySelector(".basket-meter");
  let addToBasket = function () {
    sum++;
    basketMeter.innerText = sum;
    basketMeter.style.textAlign = "center";
    basketMeter.style.color = "#F96900";
    basketMeter.style.textJustify = "center";
    if (sum >= 1) {
      stickyBasket.style.display = "block";
    } else {
      stickyBasket.style.display = "none";
    }
    toBasketButton.removeEventListener("click", addToBasket);
  };

  function sendToBasket() {
    productPopUp.style.display = "none";
    addToBasket();
    let prodForBasket = new CurrentProd(
      productId,
      popupTitle,
      popupImg,
      pizzaDough,
      pizzaSize
    );
    console.log(prodForBasket);
    shoppingCart.push(prodForBasket);
    console.log(shoppingCart);
    toBasketButton.removeEventListener("click", sendToBasket);
  }
  toBasketButton.addEventListener("click", sendToBasket);
  dodoMain.append(stickyBasket);
};

function makeVendorCode() {}
let dodoMain = document.querySelector(".dodo-menu");

// ------------------------создание продукта----------------------------

let createProduct = function (elem) {
  elem.pizzaSize = "medium";
  elem.pizzaDough = "traditional";

  let productCard = document.createElement("div");
  productCard.classList.add("dodo-productCard");
  productCard.classList.add("dodo-productCard--border");
  productCard.classList.add("dodo-productCard--padding");

  let productCardImg = document.createElement("img");
  productCardImg.src = elem.pizzaSizes[1].img.traditional;
  productCardImg.classList.add("dodo-productCard__img");
  productCard.append(productCardImg);

  let productCardInfo = document.createElement("div");
  productCardInfo.classList.add("dodo-productCard__info");
  productCard.append(productCardInfo);

  let productCardTitle = document.createElement("h2");
  productCardTitle.classList.add("dodo-productCard__title");
  productCardTitle.innerText = elem.title;

  productCardInfo.append(productCardTitle);

  let productCardIngredients = document.createElement("p");
  productCardIngredients.classList.add("dodo-productCard__description");

  elem.ingredients.map((elem) => {
    let ingredientsSpan = document.createElement("span");
    ingredientsSpan.innerText = `${elem.name}, `;
    productCardIngredients.append(ingredientsSpan);
  });
  // let template = `от ${elem.title}`

  productCardInfo.append(productCardIngredients);

  let productCardButtonPrice = document.createElement("button");
  productCardButtonPrice.classList.add("dodo-button-price");
  productCardButtonPrice.innerText = `от ${elem.pizzaSizes[1].price}₽`;
  // let template = `от ${elem.title}`

  productCardButtonPrice.id = elem.id;

  productCardInfo.append(productCardButtonPrice);
  let productCardPriceInf = document.createElement("div");
  productCardPriceInf.classList.add("dodo-productCard__priceInfo");
  productCardInfo.append(productCardPriceInf);

  let productCardPriceTag = document.createElement("p");
  productCardPriceTag.classList.add("dodo-productCard__priceTag");
  productCardPriceTag.innerText = `от ${elem.pizzaSizes[1].price}₽`;

  productCardPriceInf.append(productCardPriceTag);

  let productCardButtonChoose = document.createElement("button");
  productCardButtonChoose.classList.add("dodo-button-choose");
  productCardButtonChoose.innerText = "Выбрать";
  productCardPriceInf.append(productCardButtonChoose);
  dodoMain.append(productCard);
};
products.map((elem) => createProduct(elem));

// ---------отрисовка попапа по id ------------------
let productButtonPrices = document.querySelectorAll(".dodo-button-price");
let findId = function (evt) {
  let buttonId = evt.target.id;
  let productById = products.find((elem) => elem.id === +buttonId);
  createPopUp(productById);

  console.log(productById);
};

Array.from(productButtonPrices).map((elem) =>
  elem.addEventListener("click", findId)
);
